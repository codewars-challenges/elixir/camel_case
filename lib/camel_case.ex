defmodule CamelCase do
  def to_camel_case(str) do
    str |> String.split(["-", "_"], trim: true) |> Enum.with_index() |> Enum.map(
         fn {s, i} -> case i do
                        0 ->
                          first = String.at(s, 0)
                          rest = s |> String.capitalize() |> String.slice(1..-1)
                          "#{first}#{rest}"
                        _ ->
                          String.capitalize(s)
                      end
         end
       )
    |> Enum.join()
  end
end